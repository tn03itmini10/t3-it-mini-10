package com.college.pojo;

public class ParcelList {
    private String p_id;
    private String u_id;
    private String d_id;
    private String p_title;
    private String p_source;
    private String p_destination;
    private String p_weight;
    private String p_price;
    private String p_vehicle_type;
    private String p_status;
    private String p_time;
    private String d_name;
    private String d_latitude;
    private String d_longitude;

    public ParcelList(String p_id, String u_id, String d_id, String p_title, String p_source, String p_destination, String p_weight, String p_price, String p_vehicle_type, String p_status, String p_time, String d_name, String d_latitude, String d_longitude) {
        this.p_id = p_id;
        this.u_id = u_id;
        this.d_id = d_id;
        this.p_title = p_title;
        this.p_source = p_source;
        this.p_destination = p_destination;
        this.p_weight = p_weight;
        this.p_price = p_price;
        this.p_vehicle_type = p_vehicle_type;
        this.p_status = p_status;
        this.p_time = p_time;
        this.d_name = d_name;
        this.d_latitude = d_latitude;
        this.d_longitude = d_longitude;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getD_id() {
        return d_id;
    }

    public void setD_id(String d_id) {
        this.d_id = d_id;
    }

    public String getP_title() {
        return p_title;
    }

    public void setP_title(String p_title) {
        this.p_title = p_title;
    }

    public String getP_source() {
        return p_source;
    }

    public void setP_source(String p_source) {
        this.p_source = p_source;
    }

    public String getP_destination() {
        return p_destination;
    }

    public void setP_destination(String p_destination) {
        this.p_destination = p_destination;
    }

    public String getP_weight() {
        return p_weight;
    }

    public void setP_weight(String p_weight) {
        this.p_weight = p_weight;
    }

    public String getP_price() {
        return p_price;
    }

    public void setP_price(String p_price) {
        this.p_price = p_price;
    }

    public String getP_vehicle_type() {
        return p_vehicle_type;
    }

    public void setP_vehicle_type(String p_vehicle_type) {
        this.p_vehicle_type = p_vehicle_type;
    }

    public String getP_status() {
        return p_status;
    }

    public void setP_status(String p_status) {
        this.p_status = p_status;
    }

    public String getP_time() {
        return p_time;
    }

    public void setP_time(String p_time) {
        this.p_time = p_time;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }

    public String getD_latitude() {
        return d_latitude;
    }

    public void setD_latitude(String d_latitude) {
        this.d_latitude = d_latitude;
    }

    public String getD_longitude() {
        return d_longitude;
    }

    public void setD_longitude(String d_longitude) {
        this.d_longitude = d_longitude;
    }
}
