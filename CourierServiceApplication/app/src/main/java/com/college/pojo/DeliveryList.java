package com.college.pojo;

public class DeliveryList {
    private String d_id;
    private String d_latitude;
    private String d_longitude;

    public DeliveryList(String d_id, String d_latitude, String d_longitude) {
        this.d_id = d_id;
        this.d_latitude = d_latitude;
        this.d_longitude = d_longitude;
    }

    public String getD_id() {
        return d_id;
    }

    public void setD_id(String d_id) {
        this.d_id = d_id;
    }

    public String getD_latitude() {
        return d_latitude;
    }

    public void setD_latitude(String d_latitude) {
        this.d_latitude = d_latitude;
    }

    public String getD_longitude() {
        return d_longitude;
    }

    public void setD_longitude(String d_longitude) {
        this.d_longitude = d_longitude;
    }
}
