package com.college.courierserviceapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.college.util.SharedPreference;

public class MainActivity extends AppCompatActivity {

    CardView cardView_add_parcel,cardView_see_parcel,cardView_logout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Menu");
        cardView_add_parcel=findViewById(R.id.cd_add_parcel);
        cardView_see_parcel=findViewById(R.id.cd_see_parcel);
        cardView_logout=findViewById(R.id.cd_logout);


        cardView_add_parcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(MainActivity.this,AddParcelActivity.class);
                startActivity(i);
                finish();
            }
        });
        cardView_see_parcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(MainActivity.this,SeeParcelActivity.class);
                startActivity(i);
                finish();
            }
        });

        cardView_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPreference.contains("u_id")){
                    SharedPreference.removeKey("u_id");
                    Intent i=new Intent(MainActivity.this,LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        });


    }
}