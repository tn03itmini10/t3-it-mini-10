package com.college.courierserviceapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.ParcelListAdapter;
import com.college.pojo.ParcelList;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SeeParcelActivity extends AppCompatActivity {


    ArrayList<ParcelList> list=new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressBar progressBar;
    RecyclerView recyclerView_parcel;
    ParcelListAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_parcel);
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Parcel List");
        String u_id=SharedPreference.get("u_id");
        swipeRefreshLayout=findViewById(R.id.sw_data);
        progressBar=findViewById(R.id.progress_list);
        recyclerView_parcel=findViewById(R.id.recycler_parcel_list);
        LinearLayoutManager layoutManager=new LinearLayoutManager(SeeParcelActivity.this);
        recyclerView_parcel.setHasFixedSize(true);
        recyclerView_parcel.setLayoutManager(layoutManager);
        parcelList(u_id);



    }

    private void parcelList(String u_id) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.SEE_PARCEL_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Nik", response);
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            list.add(new ParcelList(
                                    jsonObject1.getString("p_id"),
                                    jsonObject1.getString("u_id"),
                                    jsonObject1.getString("d_id"),
                                    jsonObject1.getString("p_title"),
                                    jsonObject1.getString("p_source"),
                                    jsonObject1.getString("p_destination"),
                                    jsonObject1.getString("p_weight"),
                                    jsonObject1.getString("p_price"),
                                    jsonObject1.getString("p_vehicle_type"),
                                    jsonObject1.getString("p_status"),
                                    jsonObject1.getString("p_time"),
                                    jsonObject1.getString("d_name"),
                                    jsonObject1.getString("d_latitude"),
                                    jsonObject1.getString("d_longitude")));

                        }
                        adapter = new ParcelListAdapter(SeeParcelActivity.this,list);
                        recyclerView_parcel.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                    } else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                swipeRefreshLayout.setRefreshing(false);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(SeeParcelActivity.this, "Technical Problem arises", Toast.LENGTH_SHORT).show();
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("u_id",u_id);
                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            Intent intent=new Intent(SeeParcelActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(SeeParcelActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}