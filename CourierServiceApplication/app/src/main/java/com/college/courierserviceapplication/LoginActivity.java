package com.college.courierserviceapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText editText_email,editText_password;
    Button button_login;
    TextView tv_register;
    ProgressBar progressBar;
    FusedLocationProviderClient client;
    LocationCallback callback;
    Location location;
    LocationRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editText_email=findViewById(R.id.edt_email);
        editText_password=findViewById(R.id.edt_pass);
        button_login=findViewById(R.id.btn_login);
        tv_register=findViewById(R.id.txt_register);

        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });


        client = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        callback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                //to get location result
                location = locationResult.getLocations().get(0);
                gpsLocation();
                super.onLocationResult(locationResult);
            }
        };
        request = new LocationRequest();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(5000);
        request.setFastestInterval(3000);

        if (checkPermissionStatus())
            startLocation();

        progressBar=findViewById(R.id.progress_login);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String u_email=editText_email.getText().toString().trim();
                String u_password=editText_password.getText().toString().trim();
                if (u_email.equals("") || u_password.equals("")){
                    Toast.makeText(LoginActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else if(!Patterns.EMAIL_ADDRESS.matcher(u_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else {
                    login(u_email,u_password);
                }
            }
        });

    }

    private void login(String u_email, String u_password) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("nik","login=>"+response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        String u_id=jsonObject1.getString("u_id");
                        String u_email=jsonObject1.getString("u_email");

                        SharedPreference.save("u_id",u_id);
                        SharedPreference.save("u_email",u_email);
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_email",u_email);
                params.put("u_password",u_password);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private boolean checkPermissionStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
                requestPermissions(permissions, 123);
            } else {

            }
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==123){
            if (grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getApplicationContext(), "You have to accept permission", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void startLocation() {
        client.requestLocationUpdates(request, callback, Looper.myLooper());
    }
    private void gpsLocation() {
        Geocoder geocoder=new Geocoder(LoginActivity.this);
        try{
            List<Address> alAd=geocoder.getFromLocation(location.getLatitude(),location.getLongitude(),1);
            Address ad=alAd.get(0);
            String address=ad.getAddressLine(0);
            Log.i("nik",address);
            String coordinate=location.getLatitude() + "," + location.getLongitude();
            Log.i("nik",coordinate);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clear() {
        editText_email.setText("");
        editText_password.setText("");
    }
}