package com.college.courierserviceapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.util.AppController;
import com.college.util.Keys;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    EditText editText_name,editText_email,editText_password,editText_c_pass, editText_phone,editText_address;
    Button button_register;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editText_name=findViewById(R.id.edt_name);
        editText_email=findViewById(R.id.edt_email);
        editText_password=findViewById(R.id.edt_pass);
        editText_c_pass=findViewById(R.id.edt_conf_pass);
        editText_phone=findViewById(R.id.edt_phone);
        editText_address=findViewById(R.id.edt_address);
        button_register=findViewById(R.id.btn_register);
        progressBar=findViewById(R.id.progress_login);


        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String u_name=editText_name.getText().toString().trim();
                String u_email=editText_email.getText().toString().trim();
                String u_password=editText_password.getText().toString().trim();
                String d_conf_pass=editText_c_pass.getText().toString().trim();
                String u_phone=editText_phone.getText().toString().trim();
                String u_address=editText_address.getText().toString().trim();


                if (u_name.equals("") || u_email.equals("") || u_password.equals("") || d_conf_pass.equals("") || u_phone.equals("") || u_address.equals("")){
                    Toast.makeText(RegisterActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(u_email).matches()){
                    editText_email.setError("Invalid Email ID");
                    Toast.makeText(RegisterActivity.this, "Invalid Email ID", Toast.LENGTH_SHORT).show();
                }else if (u_password.length()<6){
                    editText_password.setError("Password must be at least 6 characters");
                    Toast.makeText(RegisterActivity.this, "Password must be at least 6 characters", Toast.LENGTH_SHORT).show();
                }else if(!d_conf_pass.equals(u_password)){
                    editText_c_pass.setError("Please Confirm the password");
                    Toast.makeText(RegisterActivity.this, "Please Confirm the password", Toast.LENGTH_SHORT).show();
                }else if(u_phone.length()!=10){
                    editText_phone.setError("Phone number must be 10 digits");
                    Toast.makeText(RegisterActivity.this, "Phone number must be 10 digits", Toast.LENGTH_SHORT).show();
                }else if(u_address.equals("")){
                        editText_address.setError("Please enter a address");
                        Toast.makeText(RegisterActivity.this, "Please enter a address", Toast.LENGTH_SHORT).show();
                }else {
                    register(u_name,u_email,u_password,u_phone,u_address);
                }
            }
        });


    }


    private void register(final String u_name, final String u_email, final String u_password, final String u_phone, String u_address) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(RegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(RegisterActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(RegisterActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_name",u_name);
                params.put("u_email",u_email);
                params.put("u_password",u_password);
                params.put("u_phone",u_phone);
                params.put("u_address",u_address);
                return params;
            }
        };

        AppController.getInstance().add(request);
    }


    private void clear() {
        editText_name.setText("");
        editText_email.setText("");
        editText_password.setText("");
        editText_c_pass.setText("");
        editText_phone.setText("");
        editText_address.setText("");


    }
}