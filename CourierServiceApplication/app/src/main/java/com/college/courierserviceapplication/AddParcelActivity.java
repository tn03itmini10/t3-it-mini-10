package com.college.courierserviceapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.adapter.PlaceAutoSuggestAdapter;
import com.college.pojo.DeliveryList;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddParcelActivity extends AppCompatActivity {
    AutoCompleteTextView autoCompleteTextView_source,autoCompleteTextView_destination;
    Button button_add_parcel,button_show;
    EditText editText_weight,editText_distance,editText_source,editText_destination,editText_title;
    Spinner sp_vehicle_type,sp_weight;
    TextView tv_distance,tv_total;
    ArrayList<String> vehicleType=new ArrayList<>();
    ArrayAdapter adapter;
    LatLng latLng_location;
    ProgressBar progressBar;
    ArrayList<DeliveryList> list=new ArrayList<>();
    FusedLocationProviderClient client;
    LocationCallback callback;
    Location location;
    LocationRequest request;
    String p_vehicle_type;
    int total,distance;
    ArrayList<String> weightList=new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_parcel);
        getSupportActionBar().setTitle("Add Parcel");
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editText_source=findViewById(R.id.edt_source);
        editText_destination=findViewById(R.id.edt_destination);
        button_add_parcel=findViewById(R.id.btn_add_parcel);
        tv_distance=findViewById(R.id.txt_distance);
        editText_weight=findViewById(R.id.edt_weight);
        editText_distance=findViewById(R.id.edt_distance);
        sp_vehicle_type=findViewById(R.id.spinner_vehicle);
        editText_title=findViewById(R.id.edt_title);
        progressBar=findViewById(R.id.progress_add_parcel);
        tv_total=findViewById(R.id.txt_total);
        button_show=findViewById(R.id.btn_show_total);
        sp_weight=findViewById(R.id.spinner_weight);
        vehicleType.add("Select Vehicle Type");
        vehicleType.add("Truck");
        vehicleType.add("Tempo");
//        weightList.add("Select Weight");
//        weightList.add("1");
//        weightList.add("2");
//        weightList.add("3");
//        weightList.add("4");
//        weightList.add("5");
//        weightList.add("10");
//        weightList.add("15");
//        weightList.add("20");
//        ArrayAdapter weightadapter=new ArrayAdapter(AddParcelActivity.this, android.R.layout.simple_spinner_dropdown_item,weightList);
//        sp_weight.setAdapter(weightadapter);

        adapter=new ArrayAdapter(AddParcelActivity.this, android.R.layout.simple_spinner_dropdown_item,vehicleType);
        sp_vehicle_type.setAdapter(adapter);

        client = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        callback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                //to get location result
                location = locationResult.getLocations().get(0);
                gpsLocation();
                super.onLocationResult(locationResult);
            }
        };
        request = new LocationRequest();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(5000);
        request.setFastestInterval(3000);

        if (checkPermissionStatus())
            startLocation();

        button_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText_weight.getText().toString().equals("")) {
                    Toast.makeText(AddParcelActivity.this, "Please enter a weight", Toast.LENGTH_SHORT).show();
                } else if (sp_vehicle_type.getSelectedItemPosition() == 0) {
                    Toast.makeText(AddParcelActivity.this, "Please select vehicle type", Toast.LENGTH_LONG).show();
                } else {
                    StringRequest request1 = new StringRequest(Request.Method.POST, Keys.URL.DELIVERY_LIST, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("nik", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("success").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        list.add(new DeliveryList(
                                                jsonObject1.getString("d_id"),
                                                jsonObject1.getString("d_latitude"),
                                                jsonObject1.getString("d_longitude")));
                                        String lat1 = jsonObject1.getString("d_latitude");
                                        String long1 = jsonObject1.getString("d_longitude");
                                        String d_id = jsonObject1.getString("d_id");

                                        client = LocationServices.getFusedLocationProviderClient(AddParcelActivity.this);
                                        int finalI = i;
                                        callback = new LocationCallback() {
                                            @Override
                                            public void onLocationResult(LocationResult locationResult) {
                                                location = locationResult.getLocations().get(0);
                                                String latitude = String.valueOf(location.getLatitude());
                                                String longitude = String.valueOf(location.getLongitude());
                                                double di = distance(Double.parseDouble(latitude), Double.parseDouble(longitude), Double.parseDouble(lat1), Double.parseDouble(long1));
                                                double dikm = di * 1.609344;
                                                double d = dikm;
                                                Log.i("nik", di + "<=distance" + finalI);

                                                if (d <= 5) {
                                                    if (editText_weight.getText().toString().equals("")) {
                                                        Toast.makeText(AddParcelActivity.this, "Please enter a weight", Toast.LENGTH_SHORT).show();
                                                    } else if (sp_vehicle_type.getSelectedItemPosition() == 0) {
                                                                Toast.makeText(AddParcelActivity.this, "Please select vehicle type", Toast.LENGTH_LONG).show();
                                                    } else {
//                                                    tv_total.setVisibility(View.VISIBLE);
                                                    String weight = editText_weight.getText().toString().trim();
                                                    String type = sp_vehicle_type.getSelectedItem().toString();
//                                                    DecimalFormat threeDForm = new DecimalFormat("######");
//                                                    String stringValue = Double.valueOf(threeDForm.format(d * 10000)).toString();
//                                                    stringValue.replaceAll(".", "");
//                                                    Log.i("calculate",stringValue);
                                                    distance = Integer.parseInt(String.valueOf(d).split("\\.")[0]);
                                                    Log.i("distance", String.valueOf(distance));
                                                        if (type.equals("Truck")) {
                                                            total = 2 * Integer.parseInt(weight) + distance;
                                                            Log.i("nik", String.valueOf(total));
                                                            tv_total.setText("Total: " + String.valueOf(total) + " Rs");
                                                        } else if (type.equals("Tempo")) {
                                                            total = 2 * Integer.parseInt(weight) + distance;
                                                            Log.i("nik", String.valueOf(total));
                                                            tv_total.setText("Total: " + String.valueOf(total) + " Rs");
                                                        }
//                                                    if (weight.equals("1") && type.equals("Truck")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//                                                    } else if (weight.equals("1") && type.equals("Tempo")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("2") && type.equals("Truck")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("2") && type.equals("Tempo")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("3") && type.equals("Truck")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("3") && type.equals("Tempo")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("4") && type.equals("Truck")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("4") && type.equals("Tempo")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("5") && type.equals("Truck")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("5") && type.equals("Tempo")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//                                                    }else if (weight.equals("10") && type.equals("Truck")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("10") && type.equals("Tempo")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//                                                    }else if (weight.equals("15") && type.equals("Truck")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("15") && type.equals("Tempo")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//                                                    }else if (weight.equals("20") && type.equals("Truck")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("20") && type.equals("Tempo")) {
//                                                        total = 2 * Integer.parseInt(weight) + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//                                                    }

                                                    }

                                                }else {

                                                }


                                                super.onLocationResult(locationResult);
                                            }

                                        };
                                        request = new LocationRequest();
                                        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                        if (ActivityCompat.checkSelfPermission(AddParcelActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddParcelActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                            return;
                                        }
                                        client.requestLocationUpdates(request, callback, null);
                                    }

                                } else {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(AddParcelActivity.this, "Technical Problem arises", Toast.LENGTH_SHORT).show();
                            error.printStackTrace();

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            return params;
                        }
                    };
                    AppController.getInstance().add(request1);


                }
            }
        });


        button_add_parcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p_title=editText_title.getText().toString().trim();
                String p_source=editText_source.getText().toString().trim();
                String p_destination=editText_destination.getText().toString().trim();
                String p_weight=editText_weight.getText().toString().trim();
                String p_price=tv_total.getText().toString().trim();
                p_vehicle_type=sp_vehicle_type.getSelectedItem().toString();

                if (p_title.equals("") || p_source.equals("") || p_destination.equals("") || p_weight.equals("") || sp_vehicle_type.getSelectedItemPosition()==0){
                    Toast.makeText(AddParcelActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (p_title.equals("")){
                    editText_title.setError("Please enter parcel title");
                }else if (p_source.equals("")){
                    editText_source.setError("Please enter source address");
                }else if (p_destination.equals("")){
                    editText_destination.setError("Please enter destination address");
                }else if ( p_weight.equals("")){
                    editText_weight.setError("Please enter a weight");
                }else if (sp_vehicle_type.getSelectedItemPosition()==0){
                    Toast.makeText(AddParcelActivity.this, "Please select vehicle type", Toast.LENGTH_SHORT).show();
                }else{
                    loadDeliveryBoyList();
                }


            }
        });
//        autoCompleteTextView_source.setAdapter(new PlaceAutoSuggestAdapter(AddParcelActivity.this,android.R.layout.simple_list_item_1));
//
//        autoCompleteTextView_source.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Log.d("Address : ",autoCompleteTextView_source.getText().toString());
//                LatLng latLng=getLatLngFromAddress(autoCompleteTextView_source.getText().toString());
//                if(latLng!=null) {
//                    Log.d("Lat Lng : ", " " + latLng.latitude + " " + latLng.longitude);
//                    Address address=getAddressFromLatLng(latLng);
//                    if(address!=null) {
//                        Log.d("Address : ", "" + address.toString());
//                        Log.d("Address Line : ",""+address.getAddressLine(0));
//                        Log.d("Phone : ",""+address.getPhone());
//                        Log.d("Pin Code : ",""+address.getPostalCode());
//                        Log.d("Feature : ",""+address.getFeatureName());
//                        Log.d("More : ",""+address.getLocality());
//                    }
//                    else {
//                        Log.d("Adddress","Address Not Found");
//                    }
//                }
//                else {
//                    Log.d("Lat Lng","Lat Lng Not Found");
//                }
//
//            }
//        });

    }



    private void loadDeliveryBoyList() {
        StringRequest request1 = new StringRequest(Request.Method.POST, Keys.URL.DELIVERY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("nik", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            list.add(new DeliveryList(
                                    jsonObject1.getString("d_id"),
                                    jsonObject1.getString("d_latitude"),
                                    jsonObject1.getString("d_longitude")));
                            String lat1=jsonObject1.getString("d_latitude");
                            String long1=jsonObject1.getString("d_longitude");
                            String d_id=jsonObject1.getString("d_id");

                            client = LocationServices.getFusedLocationProviderClient(AddParcelActivity.this);
                            int finalI = i;
                            callback = new LocationCallback() {
                                @Override
                                public void onLocationResult(LocationResult locationResult) {
                                    location = locationResult.getLocations().get(0);
                                    String latitude = String.valueOf(location.getLatitude());
                                    String longitude = String.valueOf(location.getLongitude());
                                    double di = distance(Double.parseDouble(latitude), Double.parseDouble(longitude), Double.parseDouble(lat1), Double.parseDouble(long1));
                                    double dikm = di * 1.609344;
                                    double d = dikm;
                                    Log.i("nik", di + "<=distance"+ finalI);

                                    if (d<=5){

                                        addParcel(editText_source.getText().toString().trim(), editText_destination.getText().toString().trim(), editText_weight.getText().toString().trim(),String.valueOf(total), String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()),d_id,sp_vehicle_type.getSelectedItem().toString(),editText_title.getText().toString().trim());

//                                        button_show.setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view) {
//                                                Log.i("click","button click");
//                                                if (editText_weight.getText().toString().equals("")) {
//                                                    Toast.makeText(AddParcelActivity.this, "Please enter a weight", Toast.LENGTH_SHORT).show();
//                                                } else if (sp_vehicle_type.getSelectedItemPosition() == 0) {
//                                                    Toast.makeText(AddParcelActivity.this, "Please select vehicle type", Toast.LENGTH_LONG).show();
//                                                } else {
////                                                    tv_total.setVisibility(View.VISIBLE);
//                                                    String weight = editText_weight.getText().toString().trim();
//                                                    String type = sp_vehicle_type.getSelectedItem().toString();
//                                                    distance = Integer.parseInt(String.valueOf(d).split("\\.")[0]);
//                                                    if (weight.equals("1") && type.equals("Truck")) {
//                                                        total = 2 * 1 + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("1") && type.equals("Tempo")) {
//                                                        total = 2 * 1 + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("2") && type.equals("Truck")) {
//                                                        total = 2 * 2 + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("2") && type.equals("Tempo")) {
//                                                        total = 2 * 2 + distance;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("3") && type.equals("Truck")) {
//                                                        total = 2 * 3 + 5;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("3") && type.equals("Tempo")) {
//                                                        total = 2 * 3 + 5;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("4") && type.equals("Truck")) {
//                                                        total = 2 * 4 + 5;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("4") && type.equals("Tempo")) {
//                                                        total = 2 * 4 + 5;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("5") && type.equals("Truck")) {
//                                                        total = 2 * 5 + 5;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    } else if (weight.equals("5") && type.equals("Tempo")) {
//                                                        total = 2 * 5 + 5;
//                                                        Log.i("nik", String.valueOf(total));
//                                                        tv_total.setText("Total: " + String.valueOf(total) + " Rs");
//
//                                                    }
//                                                }
//
//
//                                            }
//                                        });
                                    }



                                    super.onLocationResult(locationResult);
                                }

                            };
                            request = new LocationRequest();
                            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                            if (ActivityCompat.checkSelfPermission(AddParcelActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddParcelActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            client.requestLocationUpdates(request, callback, null);
                        }

                    } else {

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AddParcelActivity.this, "Technical Problem arises", Toast.LENGTH_SHORT).show();
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        AppController.getInstance().add(request1);
    }
    private void addParcel(String p_source, String p_destination, String p_weight, String p_price, String p_latitude, String p_longitude, String d_id, String v_type,String title) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.ADD_PARCEL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("nik","login=>"+response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        clear();
                        Toast.makeText(AddParcelActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                    }else {
                        Toast.makeText(AddParcelActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(AddParcelActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_id",SharedPreference.get("u_id"));
                params.put("d_id",d_id);
                params.put("p_source",p_source);
                params.put("p_destination",p_destination);
                params.put("p_weight",p_weight);
                params.put("p_price",p_price);
                params.put("p_vehicle_type",v_type);
                params.put("p_title",title);
//                if (p_vehicle_type.equals("Truck")){
//                    params.put("p_vehicle_type","1");
//                }else if (p_vehicle_type.equals("Tempo")){
//                    params.put("p_vehicle_type","2");
//                }
                params.put("p_latitude",p_latitude);
                params.put("p_longitude",p_longitude);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private void clear() {
        editText_destination.setText("");
        editText_weight.setText("");
        tv_total.setText("");
    }

    private LatLng getLatLngFromAddress(String address){

        Geocoder geocoder=new Geocoder(AddParcelActivity.this);
        List<Address> addressList;

        try {
            addressList = geocoder.getFromLocationName(address, 1);
            if(addressList!=null){
                Address singleaddress=addressList.get(0);
                LatLng latLng=new LatLng(singleaddress.getLatitude(),singleaddress.getLongitude());
                return latLng;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    private Address getAddressFromLatLng(LatLng latLng){
        Geocoder geocoder=new Geocoder(AddParcelActivity.this);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 5);
            if(addresses!=null){
                Address address=addresses.get(0);
                return address;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    private boolean checkPermissionStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
                requestPermissions(permissions, 123);
            } else {

            }
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==123){
            if (grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getApplicationContext(), "You have to accept permission", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void startLocation() {
        client.requestLocationUpdates(request, callback, Looper.myLooper());
    }
    private void gpsLocation() {
        Geocoder geocoder=new Geocoder(AddParcelActivity.this);
        try{
            List<Address> alAd=geocoder.getFromLocation(location.getLatitude(),location.getLongitude(),1);
            Address ad=alAd.get(0);
            String address=ad.getAddressLine(0);
            //Log.i("nik",address);
            String coordinate=location.getLatitude() + "," + location.getLongitude();
            String latitude= String.valueOf(location.getLatitude());
            String longitude= String.valueOf(location.getLongitude());
            editText_source.setText(address);
            //Log.i("nik",coordinate);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        Log.i("nik", String.valueOf(dist)+"=>dist");

        return (dist);
    }


    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            Intent intent=new Intent(AddParcelActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(AddParcelActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}