package com.college.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.college.courierserviceapplication.R;
import com.college.pojo.ParcelList;
import com.college.util.AppController;
import com.college.util.Keys;
import com.college.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ParcelListAdapter extends RecyclerView.Adapter<ParcelListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ParcelList> list;

    public ParcelListAdapter(Context context, ArrayList<ParcelList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.custom_parcel, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ParcelList parcelList=list.get(position);
        holder.tv_title.setText(parcelList.getP_title());
        holder.tv_source.setText("Source :- "+parcelList.getP_source());
        holder.tv_destination.setText("Destination :- "+parcelList.getP_destination());
        holder.tv_weight.setText("Weight: "+parcelList.getP_weight() +" Kg");
        holder.tv_price.setText("Price: "+parcelList.getP_price()+" Rs");
        //holder.tv_source.setPaintFlags(holder.tv_source.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.tv_source.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayTrack(parcelList.getP_source(),parcelList.getP_destination());
            }
        });

        if (parcelList.getP_status().equals("0")){
            holder.tv_status.setText("Status: Pending");
        }else if (parcelList.getP_status().equals("1")){
            holder.tv_status.setText("Status: Pickup");
        }else if (parcelList.getP_status().equals("2")){
            holder.tv_status.setText("Status: Delivered");
        }

        holder.tv_vehicle_type.setText("Vehicle Type: "+parcelList.getP_vehicle_type());
        holder.tv_time.setText(parcelList.getP_time());

        holder.tv_name.setText("Name: "+parcelList.getD_name());
        holder.imageView_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Uri uri=Uri.parse("http://maps.google.com/?q="+parcelList.getD_latitude()+","+parcelList.getD_longitude());
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.setPackage("com.google.android.apps.maps");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }catch (ActivityNotFoundException e){
                    Uri uri=Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps");
                    Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });

    }

    private void displayTrack(String p_source, String p_destination) {
        try {
            Uri uri=Uri.parse("https://www.google.co.in/maps/dir/"+p_source+"/"+p_destination);
            Intent intent=new Intent(Intent.ACTION_VIEW,uri);
            intent.setPackage("com.google.android.apps.maps");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }catch (ActivityNotFoundException e){
            Uri uri=Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps");
            Intent intent=new Intent(Intent.ACTION_VIEW,uri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_source,tv_destination,tv_price,tv_weight,tv_title,tv_status,tv_time,tv_vehicle_type,tv_name;
        ImageView imageView_location;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_source=itemView.findViewById(R.id.txt_source);
            tv_destination=itemView.findViewById(R.id.txt_destination);
            tv_price=itemView.findViewById(R.id.txt_price);
            tv_weight=itemView.findViewById(R.id.txt_weight);
            tv_status=itemView.findViewById(R.id.txt_status);
            tv_time=itemView.findViewById(R.id.txt_time);
            tv_title=itemView.findViewById(R.id.txt_title);
            tv_vehicle_type=itemView.findViewById(R.id.txt_vehicle_type);
            imageView_location=itemView.findViewById(R.id.img_location);
            tv_name=itemView.findViewById(R.id.txt_name);


        }
    }
}
